#================================================
# Full OS image to setup env to build application
#================================================

FROM python:3.8-slim-buster AS base
LABEL base_image=true

# WARNING! You cannot have a POETRY_HOME in application 
# build directory (APP_BUILD_PATH). It messes up poetry 
#( but only under docker) and causes horrible and 
# really hard to debug issue with concurrent.futures 
# on running pytest. You will be left pulling your hair.
# /opt is used because this directory is already present
# in base image.  The more of base image you use the 
# smaller your builds will be.
ENV LANG=C.UTF-8 \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    PIP_NO_CACHE_DIR=off \
    PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=0 \
    PYTHONIOENCODING=UTF-8 \
    PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_CACHE_DIR="/opt/cache/pip" \
    APP_BUILD_PATH="/build" \
    VENV_PATH="/build/venv" \
    APP_PATH="/app" \
    DEPLOY_VIRTUALENVS_PATH="/app/venv" \
    POETRY_HOME="/opt/poetry" \
    POETRY_NO_INTERACTION=1 \
    OLD_PATH="${PATH}" \
    PATH="${POETRY_HOME}/bin:${VENV_PATH}/bin:${PATH}"

RUN apt-get -y update \
    && apt-get -y --no-install-recommends install \
    curl build-essential gcc

# Aid caching and create a blank virtual environment
RUN python -V \
    && pip install -U pip \
    && python -m venv ${VENV_PATH}

WORKDIR ${APP_PATH}
RUN python -m venv ${APP_PATH}/venv

# Download and install poetry
WORKDIR ${APP_BUILD_PATH}
RUN curl --silent --show-error --remote-name --show-error --location "https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py" 

RUN python get-poetry.py \
    && rm -f get-poetry.py

#================================================
# Install dependencies
#================================================

FROM base AS builder
LABEL base_image=false
LABEL builder_image=true

# It is important to set the previous label 
# differently to isolate the current image 
# otherwise only the last most image is captured.
LABEL base_image=false
LABEL builder_image=true

# Do not nest variables names. Doesn't work!
ENV PATH="${POETRY_HOME}/bin:${VENV_PATH}/bin:${OLD_PATH}"

COPY --from=base ${APP_BUILD_PATH} ${APP_BUILD_PATH}
COPY --from=base ${APP_PATH} ${APP_PATH}

WORKDIR ${APP_BUILD_PATH}
COPY ./poetry.lock ./pyproject.toml ./scripts/entrypoint.sh ./

# Install only runtime dependencies
RUN poetry config --local virtualenvs.create false \
    && poetry config --local virtualenvs.in-project false \
    && poetry config --local virtualenvs.path ${VENV_PATH} \
    && chmod +x ./entrypoint.sh \
    && ls -la ${APP_BUILD_PATH}

RUN poetry export --dev --format requirements.txt | pip install -r /dev/stdin

ENV PATH="${DEPLOY_VIRTUALENVS_PATH}/bin:${POETRY_HOME}/bin:${OLD_PATH}"
RUN poetry export --format requirements.txt | pip install -r /dev/stdin

#================================================
# Build and test the application
#================================================

FROM builder AS development
LABEL base_image=false
LABEL builder_image=false
LABEL development_image=true

ENV PATH="${POETRY_HOME}/bin:${VENV_PATH}/bin:${OLD_PATH}"

COPY --from=builder ${APP_BUILD_PATH} ${APP_BUILD_PATH}

WORKDIR ${APP_BUILD_PATH}
# Respects entries in .dockerignore file
COPY . . 
RUN pwd && ls -la ${APP_BUILD_PATH}

# Unit test application and run 
ENV PATH_TESTS="./tests" \
    PATH_SPEC="./config/coverage.ini"
ENV COVERAGE_THRESHOLD=90
RUN coverage run --rcfile ${PATH_SPEC} -m pytest ${PATH_TESTS}
RUN coverage report --fail-under ${COVERAGE_THRESHOLD}

#================================================
# Package the application
#================================================

FROM development AS package
LABEL base_image=false
LABEL builder_image=false
LABEL development_image=false
LABEL package_image=true

ARG COMMIT_TAG
ARG EXT_PYPI_URL
ARG EXT_PYPI_USER
ARG EXT_PYPI_PASS
ARG PACKAGE_NAME
ARG PYPI_URL

ENV PATH="${POETRY_HOME}/bin:${VENV_PATH}/bin:${OLD_PATH}"

COPY --from=development ${APP_BUILD_PATH} ${APP_BUILD_PATH}
COPY --from=builder ${APP_PATH} ${APP_PATH}

WORKDIR ${APP_BUILD_PATH}

RUN poetry version ${COMMIT_TAG} && poetry build --format wheel \
    && pwd && ls -la ${APP_BUILD_PATH} \
    && pwd && ls -la ${APP_PATH}

RUN poetry config --local repositories.ext_pypi ${EXT_PYPI_URL} \
    && poetry publish --repository ext_pypi --username ${EXT_PYPI_USER} --password ${EXT_PYPI_PASS}

# Create virtualenv for deployment. A second environment is useful to
# remove the development dependencies that were installed earlier
ENV PATH="${DEPLOY_VIRTUALENVS_PATH}/bin:${POETRY_HOME}/bin:${OLD_PATH}"
RUN pip install ${PACKAGE_NAME} --extra-index-url ${PYPI_URL}
COPY --from=development ${APP_BUILD_PATH}/entrypoint.sh ${APP_PATH}

#================================================
# Build a smaller deployment image
#================================================

FROM python:3.8-slim-buster AS deployment
LABEL base_image=false
LABEL builder_image=false
LABEL development_image=false
LABEL package_image=false

ARG APP_OBJECT

ENV LANG=C.UTF-8 \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    PIP_NO_CACHE_DIR=off \
    PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PATH="${DEPLOY_VIRTUALENVS_PATH}/bin:${PATH}" \
    APP_PATH="/app" \
    APPLICATION=${APP_OBJECT}

COPY --from=package ${APP_PATH} ${APP_PATH} 

WORKDIR ${APP_PATH}
EXPOSE 8000
ENTRYPOINT ./entrypoint.sh "gunicorn --worker-class uvicorn.workers.UvicornWorker --config python:config.gunicorn_conf" ${APPLICATION}
