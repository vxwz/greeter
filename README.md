# greeter

Explore Gitlab workflow and features while building a simple RESTful Service in Python

## TODO

- 
- [ ] Build a basic frontend without using any frameworks.
- [x] Add parameter validations to build scripts
- [x] Check if upload/download URL can be same for PyPI 
- [ ] Handle the `--password-stdin` issues
