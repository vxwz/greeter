#!/bin/bash

#=================================================
# Exit immediately if any line throws error
#=================================================
set -e

#=================================================
# Parameters for the script
#=================================================
UNDER_CICD=0  # Set default value
DOCKERFILE=backend.dockerfile

for i in "$@"
do
case $i in
    -g=*|--git_sha=*)
    GIT_SHA="${i#*=}"
    shift # past argument=value
    ;;
    -u=*|--registry_user=*)
    REGISTRY_USER="${i#*=}"
    shift # past argument=value
    ;;
    -p=*|--registry_pass=*)
    REGISTRY_PASS="${i#*=}"
    shift # past argument=value
    ;;
    -r=*|--registry=*)
    DOCKER_REGISTRY="${i#*=}"
    shift # past argument=value
    ;;
    -n=*|--upload_url=*)
    PYPI_UPLOAD_URL="${i#*=}"
    shift # past argument=value
    ;;
    -s=*|--download_url=*)
    PYPI_DOWNLOAD_URL="${i#*=}"
    shift # past argument=value
    ;;
    -a=*|--package_name=*)
    PACKAGE_NAME="${i#*=}"
    shift # past argument=value
    ;;
    -o=*|--app_object=*)
    APP_OBJECT="${i#*=}"
    shift # past argument=value
    ;;
    -t=*|--commit_tag=*)
    COMMIT_TAG="${i#*=}"
    shift # past argument=value
    ;;
    -c=*|--under_cicd=*)
    UNDER_CICD="${i#*=}"
    shift # past argument=value
    ;;
    *)    # unknown option
    ;;
esac
done

echo "==============================================="
echo "Parameters:"
echo "==============================================="
echo "GIT_SHA = ${GIT_SHA}"
echo "UNDER_CICD = ${UNDER_CICD}"
echo "COMMIT_TAG = ${COMMIT_TAG}"
echo "APP_OBJECT = ${APP_OBJECT}"
echo "PACKAGE_NAME = ${PACKAGE_NAME}"
echo "PYPI_UPLOAD_URL = ${PYPI_UPLOAD_URL}"
echo "DOCKER_REGISTRY = ${DOCKER_REGISTRY}"
echo "REGISTRY_USER = ${REGISTRY_USER}"
echo "DOCKERFILE = ${DOCKERFILE}"
echo "==============================================="

#=================================================
# Define the tags
#=================================================
BASE_TAG="${DOCKER_REGISTRY}/base:latest"
PACKAGE_TAG="${DOCKER_REGISTRY}/package:latest"
BUILDER_TAG="${DOCKER_REGISTRY}/builder:latest"
DEVELOPMENT_TAG="${DOCKER_REGISTRY}/development:latest"
DEPLOYMENT_LATEST_TAG="${DOCKER_REGISTRY}/deployment:latest"
DEPLOYMENT_COMMIT_TAG="${DOCKER_REGISTRY}/deployment:${GIT_SHA}"

echo "Tags:"
echo "==============================================="
echo "Base image tag: $BASE_TAG"
echo "Package image tag: $PACKAGE_TAG"
echo "Builder image tag: $BUILDER_TAG"
echo "Development image tag: $DEVELOPMENT_TAG"
echo "Deployment commit image tag: $DEPLOYMENT_COMMIT_TAG"
echo "Deployment latest image tag: $DEPLOYMENT_LATEST_TAG"
echo "==============================================="

#=================================================
# Login to remote docker registry
#=================================================
if [[ ${UNDER_CICD} -eq 1 ]]; then
    echo "The script is running under CI/CD environment"
else
    echo "The script is running on a local machine."
    docker login -u ${REGISTRY_USER} -p ${REGISTRY_PASS} ${DOCKER_REGISTRY} 
fi

#=================================================
# Echo commands
#=================================================
set -x

#=================================================
# Pull the images to help with caching
#=================================================
docker pull $BASE_TAG || true
docker pull $BUILDER_TAG || true
docker pull $DEVELOPMENT_TAG || true
docker pull $PACKAGE_TAG || true
docker pull $DEPLOYMENT_LATEST_TAG || true

#=================================================
# Build the package image
#=================================================
docker build \
    --file ${DOCKERFILE} \
    --target package \
    --build-arg COMMIT_TAG="${COMMIT_TAG}" \
    --build-arg EXT_PYPI_URL="${PYPI_UPLOAD_URL}" \
    --build-arg EXT_PYPI_USER="${REGISTRY_USER}" \
    --build-arg EXT_PYPI_PASS="${REGISTRY_PASS}" \
    --build-arg PYPI_URL="${PYPI_DOWNLOAD_URL}" \
    --build-arg PACKAGE_NAME="${PACKAGE_NAME}" \
    --tag "${PACKAGE_TAG}" \
    --cache-from "${DEVELOPMENT_TAG}"  \
    --cache-from "${BUILDER_TAG}"  \
    --cache-from "${BASE_TAG}"  \
    --cache-from "${PACKAGE_TAG}"  \
    .

# Push the package image to remote docker registry
docker image push "${PACKAGE_TAG}"

#=================================================
# Build the deployment image
#=================================================
docker build \
    --file ${DOCKERFILE} \
    --target deployment \
    --build-arg COMMIT_TAG="${COMMIT_TAG}" \
    --build-arg EXT_PYPI_URL="${PYPI_UPLOAD_URL}" \
    --build-arg EXT_PYPI_USER="${REGISTRY_USER}" \
    --build-arg EXT_PYPI_PASS="${REGISTRY_PASS}" \
    --build-arg PYPI_URL="${PYPI_DOWNLOAD_URL}" \
    --build-arg PACKAGE_NAME="${PACKAGE_NAME}" \
    --build-arg APP_OBJECT="${APP_OBJECT}" \
    --tag "${DEPLOYMENT_COMMIT_TAG}" \
    --tag "${DEPLOYMENT_LATEST_TAG}" \
    --cache-from "${PACKAGE_TAG}"  \
    --cache-from "${DEVELOPMENT_TAG}"  \
    --cache-from "${BUILDER_TAG}"  \
    --cache-from "${BASE_TAG}"  \
    --cache-from "${DEPLOYMENT_LATEST_TAG}" \
    .
        
# Push the deployment image to remote docker registry
docker image push "${DEPLOYMENT_COMMIT_TAG}"
docker image push "${DEPLOYMENT_LATEST_TAG}"


# Will exit with status of last command.
exit
