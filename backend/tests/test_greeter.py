from fastapi.testclient import TestClient
from greeter import __version__
from greeter.api import app

client = TestClient(app)


def test_version():
    assert __version__ == "0.1.0"


def test_greeter():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Hello, World!"}
