from fastapi import FastAPI
from greeter import core

app = FastAPI()


@app.get("/")
async def greeter():
    return {"message": core.greeting()}
