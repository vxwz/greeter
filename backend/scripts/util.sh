#!/bin/bash

#=================================================
# Exit immediately if any line throws error
#=================================================
set -e

#=================================================
# Run tests first to discover build failure early
#=================================================
PATH_TESTS="./tests"
PATH_SPEC="./config/coverage.ini"
COVERAGE_THRESHOLD=90
coverage run --rcfile ${PATH_SPEC} -m pytest ${PATH_TESTS}
coverage report --fail-under ${COVERAGE_THRESHOLD}

#=================================================
# Define the parameters
#=================================================
PROJECT_ID="22594812"
PROJECT_NAME="greeter"
REGISTRY_USER="gitlab+deploy-token-284808"
REGISTRY_PASS="fKinxMx37sxo1z1fVscz"
PRIVATE_TOKEN="xRgh-QxFRv2-zjBos6nP"
DOCKER_REGISTRY="registry.gitlab.com/vxwz/${PROJECT_NAME}"
PYPI_UPLOAD_URL="https://gitlab.com/api/v4/projects/${PROJECT_ID}/packages/pypi"
PYPI_DOWNLOAD_URL="https://${REGISTRY_USER}:${REGISTRY_PASS}@gitlab.com/api/v4/projects/${PROJECT_ID}/packages/pypi/simple"
PACKAGE_NAME=${PROJECT_NAME}
APPLICATION="${PROJECT_NAME}.api:app"
COMMIT_TAG="0.1.0"
GIT_SHA="a8c3930"
PUSH_IMAGES=0

#=================================================
# Debug information
#=================================================
echo "Registry User = ${REGISTRY_USER}"
echo "Project = ${PROJECT_ID}"
echo "PyPI Upload URL = ${PYPI_UPLOAD_URL}"
echo "PyPI Download URL = ${PYPI_DOWNLOAD_URL}"

#=================================================
# Get all packages within the repository
#=================================================
CONTENT=$( curl --header ${REGISTRY_USER}:${REGISTRY_PASS} --silent --show-error --location https://gitlab.com/api/v4/projects/${PROJECT_ID}/packages/ )

echo "${CONTENT}" | jq '.[] | {id: .id, name: .name, created: .created_at, version: .version, type: .package_type}'

#=================================================
# Get the last package Id and delete it
#=================================================
PACKAGE_ID=$(jq -r '.[] | .id' <<< "${CONTENT}")

echo "Deleting package with Id: ${PACKAGE_ID}"
RESULT=$( curl --silent --show-error --request DELETE --header PRIVATE-TOKEN:${PRIVATE_TOKEN} --location  https://gitlab.com/api/v4/projects/${PROJECT_ID}/packages/${PACKAGE_ID} )

#=================================================
# Run the build script
#=================================================
exec ./scripts/build.sh     \
    -g=${GIT_SHA}           \
    -u=${REGISTRY_USER}     \
    -p=${REGISTRY_PASS}     \
    -r=${DOCKER_REGISTRY}   \
    -n=${PYPI_UPLOAD_URL}   \
    -s=${PYPI_DOWNLOAD_URL} \
    -a=${PACKAGE_NAME}      \
    -o=${APPLICATION}       \
    -t=${COMMIT_TAG}        \
    -i=${PUSH_IMAGES}
