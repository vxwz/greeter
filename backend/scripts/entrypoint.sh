#!/bin/bash

# If a command fails, raise exception.
echo "Current Directory: `pwd` "
echo "Contents of Current Directory: `ls -la`"
echo "Arguments: $@"

set -e

# Activate the virtual environment
. /app/venv/bin/activate

# Evaluating passed command
exec $@
